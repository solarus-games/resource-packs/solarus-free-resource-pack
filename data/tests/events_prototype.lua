--[[
	This script contains various functions to facilitate unit tests of the multi_events
	script.
	
	Usage Example:
	require"multi_events"
	local events_proto = require"tests/events_prototype"
	entity:register_event("on_disabled", function() events_proto:log"first_event" end)
	entity:register_event("on_disabled", function() events_proto:log"second_event" end)
	events_proto:set_trigger(function(callback)
		entity:set_enabled(false) --causes events to trigger
		--callback is not used so function has no return: comparison of expected to results happens immediately
		--return true if intend to call callback later: comparison of expected to results happens when callback is called
	end
	events_proto:trigger("first_event;second_event") --both events expected to trigger and in this order, otherwise error
]]

local events_proto = {}

local events = {} --(table, array) list of text from each events that was triggered
local trigger_func --(function) function to call to trigger the events being tested
local running_count = 0 --number of tests waiting to finish

--// When an event is triggered it should call this function passing text to signify the event was called
	--text (string) - unique identifier for the event for tracking which events were called and in what order
		--do not include semi-colons (;) in the text
function events_proto:log(text)
	table.insert(events, text)
end

--// Pass the function to be used in order to trigger the events being tested
	--func (function) - this function will be called in order to trigger the events
		--The results are usually checked immediately after the events are triggered.
		--The function is passed an optional callback to defer the check until later,
		--where the function must return true if the callback is intended to be used,
		--otherwise the callback gets called automatically when the function returns.
function events_proto:set_trigger(func)
	assert(type(func)=="function", "Test setup error: argument #1 of `events_proto:set_trigger()` must be a function")
	trigger_func = func
end

--// Triggers the events being tested by calling the function set with 'set_trigger()`,
--// then compares expected result to actual, throwing an error if they do not match.
--// 'set_trigger()' must be called before calling this function.
	--expected_result (string) - expected result text from each event separated by semi-colons (in the order expected to be triggered)
	--msg (string, optional) - description of the error (if it occurs) to be included with error message
function events_proto:trigger(expected_result, msg)
	events = {} --clear out any previously results before triggering events
	assert(trigger_func, "Test setup error: `events_proto:set_trigger()` must be called before calling `events_proto:trigger()`")
	running_count = running_count + 1
	
	--callback function to compare expected to actual
	local function callback()
		local result = table.concat(events, ';')
		local err = "event sequence failed: expected '%s' - got '%s'"
		if msg then err = string.format("%s (%s)", err, msg) end
		assert(result==expected_result, string.format(err, expected_result, result))
		--print("test passed: "..result) --DEBUG
		running_count = running_count - 1
	end
	
	local is_cb = trigger_func(callback) --trigger the events
	
	if not is_cb then --call the callback automatically if function had no return
		callback()
	end --otherwise wait for callback to be called externally before doing comparison
end

function events_proto:exit()
	assert(running_count <= 0, string.format("%d test(s) failed to complete", running_count))
	sol.main.exit()
end

return events_proto

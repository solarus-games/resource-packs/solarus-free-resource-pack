--[[
    This unit test verifies multi-events work with user-created metatables (see issue #30)

    Only tests a metatable where __index is another table
]]

local map = ...

local multi_events = require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
    local mt = {}
    multi_events:enable(mt)
    mt:register_event("on_started", function()
        events_proto:log"meta"
    end)

    local menu = setmetatable({}, {__index=mt})
    menu:register_event("on_started", function()
        events_proto:log"menu"
    end)

    events_proto:set_trigger(function() sol.menu.start(map, menu) end)
    events_proto:trigger"meta;menu"

    events_proto:exit()
end

--[[
	This unit test for the multi_events script verifies that the
	custom_entity:on_interaction() event is triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

custom:register_event("on_interaction", function(self)
	events_proto:log(sol.main.get_type(self))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function()
		game:simulate_command_pressed"action" --hero interacts with npc
	end)
	events_proto:trigger"custom_entity"
	
	events_proto:exit() --verifies all tests have finished running before exit
end

--[[
	This unit test for the multi_events script verifies that the
	chest:on_opened() event is triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local end_test_cb --(function) call after hero teleportation is complete
my_chest:register_event("on_opened", function(self, treasure_item, treasure_variant, treasure_savegame_variable)
	local treasure_name = treasure_item:get_name()
	local data = {sol.main.get_type(self), treasure_name, treasure_variant, treasure_savegame_variable}
	events_proto:log(table.concat(data, ","))
	end_test_cb()
	events_proto:exit() --verifies all tests have finished running before exit
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function(callback)
		end_test_cb = callback
		game:simulate_command_pressed"action" --hero opens chest
		return true
	end)
	events_proto:trigger"chest,heart,1,got_chest"
	
	sol.timer.start(map, 2000, function() --if event not triggered after 2 sec then exit (test fails)
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end

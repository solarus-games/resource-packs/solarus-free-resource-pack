--[[
	This unit test for the multi_events script verifies that the
	carried_object:on_lifted() event is triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local end_test_cb
local carry_meta = sol.main.get_metatable"carried_object"
carry_meta:register_event("on_lifted", function(self) --use meta table since carried object doesn't exist on map
	events_proto:log(sol.main.get_type(self))
	end_test_cb()
	events_proto:exit() --verifies all tests have finished running before exit
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function(callback)
		end_test_cb = callback
		game:simulate_command_pressed"action" --hero lifts vase
		return true
	end)
	events_proto:trigger"carried_object"
	
	sol.timer.start(self, 2000, function() --if event not triggered after 2 sec then exit (test fails)
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
